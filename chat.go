package main

import (
	"context"
	"log"

	"gitlab.com/oladejifemi00/grpc_sample/chat"
)


type Server struct{}

func (s *Server) SayHello(ctx context.Context, msg *chat.Chat) (*chat.Chat, error) {
	log.Println("received chat request with body :: ", msg.Body)
	return &chat.Chat{Body: "Hello from our app", Count: 0}, nil
}